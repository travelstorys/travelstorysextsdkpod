Pod::Spec.new do |s|
    s.name         = "TravelStorysExtSDK"
    s.version      = "1.3.4"
    s.summary      = "TravelStorysExtSDK - Binaries"
    s.description  = "Framework for interfacing with the TravelStorys API and displaying TravelStorys tours."
    s.homepage     = "https://travelstorys.com"
    s.license = { :type => 'Copyright', :text => <<-LICENSE
                   Copyright 2020 TravelStorysGPS, LLC
                   Permission is granted to use, but not modify the code in any projects where the user has an existing license agreement with TravelStorysGPS, LLC
                  LICENSE
                }
    s.author             = { "TravelStorys" => "info@travelstorysgps.com" }
    s.source       = { :git => "https://davidtravelstorysgps@bitbucket.org/travelstorys/travelstorysextsdk.git", :tag => "#{s.version}" }
    s.public_header_files = "TravelStorysExtSDK.framework/Headers/*.h"
    s.source_files = "TravelStorysExtSDK.framework/Headers/*.h"
    s.vendored_frameworks = "TravelStorysExtSDK.framework"
    s.platform = :ios
    s.ios.deployment_target  = '8.0'
end