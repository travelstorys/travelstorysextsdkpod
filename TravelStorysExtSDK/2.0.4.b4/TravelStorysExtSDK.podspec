Pod::Spec.new do |s|
    s.name         = "TravelStorysExtSDK"
    s.version      = "2.0.4.b4"
    s.summary      = "TravelStorysExtSDK - Binaries"
    s.description  = "Framework for interfacing with the TravelStorys API and displaying TravelStorys tours."
    s.homepage     = "https://travelstorys.com"
    s.license = { :type => 'Copyright', :text => <<-LICENSE
                   Copyright 2022 TravelStorysGPS, LLC
                   Permission is granted to use, but not modify the code in any projects where the user has an existing license agreement with TravelStorysGPS, LLC
                  LICENSE
                }
    s.author             = { "TravelStorys" => "info@travelstorysgps.com" }
    s.source       = { :git => "https://davidtravelstorysgps@bitbucket.org/travelstorys/travelstorysextsdk.git", :tag => "#{s.version}" }
    s.vendored_frameworks = "TravelStorysExtSDK.xcframework"
    s.platform = :ios
    s.ios.deployment_target  = '9.0'
    s.user_target_xcconfig = { 'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'arm64' }
    s.pod_target_xcconfig = { 'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'arm64' }
    s.user_target_xcconfig = { 'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'i386' }
    s.pod_target_xcconfig = { 'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'i386' }
end